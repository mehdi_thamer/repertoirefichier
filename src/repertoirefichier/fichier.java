package repertoirefichier;

public class fichier extends Racine implements RACINES {
	
	private String nom;
	private int taille;
	
	fichier(String nom , int taille)
	{
		super(nom,"fichier");
		setNom(nom);
		settaille(taille);
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public float gettaille() {
		return taille;
	}
	
	public void settaille(int taille) {
		this.taille = taille;
	}

}

