package repertoirefichier;

import java.util.ArrayList;


public class repertoire extends Racine implements RACINES  {
	private String nom;
	private ArrayList <fichier> fichier;
	private ArrayList<repertoire> repertoire;
	
	public String getNom() 
	{
		return nom;
	}
	
	public void setNom(String nom) 
	{
		this.nom = nom;
	}
	
	repertoire(String nom)
	{
		super(nom,"repertoire");
		setNom(nom);
		fichier = new ArrayList<fichier>();
		repertoire = new ArrayList<repertoire>();		
	}
	
	public float gettaille()
	{
		int S;
		S=0;
		while (this.fichier!=null)
		{
			S=S+this.fichier.size();
		}
		
		while (this.repertoire!=null)
		{
			S=S+this.repertoire.size();
		}
		
		return S;
	}
	
	public Racine recherche(Racine e)
	{
		int i =0;
		while(i<this.fichier.size())
		{
			if(fichier.get(i).equals(e))
			{
				return((Racine )fichier.get(i));
			}
			else
			{
			    i++;
			}
		}
		return(null);
	}

	public void ajouterRep(repertoire e)
	{ 
		if( ! existeR(e))
			this.repertoire.add(e);
		else{
			System.out.println (" Repertoire existe d�ja");}
	}
	public void ajouterFichier(fichier e)
	{
		if( ! existeF(e)){
			this.fichier.add(e);}
		else{
			System.out.println (" Fichier existe d�ja");}
	}
	
	public void supprime(Racine e)
	{
		if (this.recherche(e)!=null)
		{
			if( this.repertoire.contains(e))
				this.repertoire.remove(e);
			else
				this.fichier.remove(e);}
		}
		public boolean existeR (repertoire e ) {
			boolean res= false ;
			int i = 0 ; 
			while (i<repertoire.size()&& res==false){
				if ( this.repertoire.equals(e) && this.repertoire.contains(e) ){
					res = true ;}
				else {
					res = false ;}
				i++; }
		return res ;
			}
		public boolean existeF (fichier e ) {
			boolean res= false ;
			int i = 0 ; 
			while (i<fichier.size()&& res==false){
				if ( this.fichier.contains(e) ){
					res = true ;}
				else {
					res = false ;}
				i++; }
		return res ;
			}
	}
	


